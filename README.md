# Pr�ctica 14: Problema Productor - Consumidor

Este es un repositorio con ejemplos para la pr�ctica 14 de la materia Programaci�n de Sistemas (CCPG1008) P1 de la ESPOL.

### Uso ###

El ejemplo en printmem.c calcula el porcentaje de uso de la memoria RAM cada <periodo> microsegundos.

Ejemplo:
```
./printmem 300000
```
Imprime el porcentaje de uso de memoria cada 300 ms.

### �C�mo empiezo? ###

* Hacer un fork de este repositorio a su cuenta personal de Bitbucket (una cuenta por grupo)
* Clonar el repositorio en su cuenta (no esta) en su computadora del laboratorio
* Completar la pr�ctica en grupo
* Haga commit y push a su trabajo
* El entregable es un enlace al repositorio

### Integrantes ###

Edite esta lista y a�ada los nombres de los integrantes, luego borre esta l�nea.

* Integrante 1
* Integrante 2