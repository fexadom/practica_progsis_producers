#include "csapp.h"
#include <stdio.h>

/*
Muestra el porcentaje de memoria en uso cada <periodo> microsegundos. 
*/
int main(int argc, char **argv)
{
	int periodo; //periodo de muestreo
	int memTotal; //Memoria total en el sistema
	int memFree; //Memoria libre en el sistema
	float memUso; //Porcentaje de memoria en uso
	FILE *meminfoFile;

	if (argc != 2) {
		printf("uso: %s <periodo>\n", argv[0]);
		exit(0);
	}

	periodo = atoi(argv[1]);

	meminfoFile = fopen ("/proc/meminfo","r"); //Uso /proc/meminfo para obtener información de la memoria
	if (meminfoFile == NULL)
		unix_error("No se pudo abrir /proc/meminfo");

	//Deshabilita el buffer de stdout para no esperar un salto de línea
	setbuf(stdout, NULL);
	//Deshabilita el buffer para obtener datos frescos de /proc/meminfo
	setbuf(meminfoFile, NULL);
	//Lee las primeras dos lineas de /proc/meminfo y extrae los valores de uso de memoria
	while( fscanf(meminfoFile, "MemTotal: %d kB MemFree: %d kB ", &memTotal, &memFree) == 2) {
		memUso = (((float) memTotal - (float) memFree)/ (float) memTotal) * 100.0;
		printf("\rUso: %.1f",memUso);
		//Regresa el puntero del archivo al inicio
		rewind(meminfoFile);
		usleep(periodo);
	}
	

	fclose(meminfoFile);

	return 0;
}