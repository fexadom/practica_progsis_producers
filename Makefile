CC = gcc
CFLAGS = -g -O2 -Wall -I .

# This flag includes the Pthreads library on a Linux box.
# Others systems will probably require something different.
LIB = -lpthread

.PHONY: all
all: printmem

printmem: printmem.c csapp.o
	$(CC) $(CFLAGS) -o printmem printmem.c csapp.o $(LIB)	

csapp.o: csapp.c
	$(CC) $(CFLAGS) -c csapp.c

.PHONY: clean
clean:
	rm -f *.o printmem *~

